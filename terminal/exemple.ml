open Curses

(* toutes les fonctions et les types sont accessibles *)
(* La doc est ici : https://www.nongnu.org/ocaml-tmk/doc/Curses.html *)
(* mais c'est assez obscur, il y a une doc plus claire ici :
    https://www.ibm.com/docs/en/aix/7.1?topic=concepts-curses-library
  les noms des fonctions sont les mêmes *)

(* On doit commencer par définir nos couleurs *)
let noir = 0
let gris = noir+1
let blanc = gris+1
let rouge = blanc+1
let rouge_clair = rouge+1
let vert = rouge_clair+1
let vert_clair = vert+1
let bleu = vert_clair+1
let bleu_clair = bleu+1

let ncolors = bleu_clair + 1

let cree_couleurs () =
    assert(init_color noir 0 0 0);
    assert(init_color gris 500 500 500);
    assert(init_color blanc 1000 1000 1000);
    assert(init_color rouge 1000 0 0);
    assert(init_color rouge_clair 1000 500 500);
    assert(init_color vert 0 1000 0);
    assert(init_color vert_clair 500 1000 500);
    assert(init_color bleu 0 0 1000);
    assert(init_color bleu_clair 500 500 1000)

let _ =
    let w = initscr () in
    assert(nodelay w true);
    assert(keypad w true);
    assert (start_color ());
    assert (cbreak ());
    assert (noecho ())

let cree_paires () =
    let paires = Array.make_matrix ncolors ncolors 0 in
    let p = ref 10 in
    for i = 0 to ncolors-1 do
        for j = 0 to ncolors-1 do
            assert(init_pair !p i j);
            paires.(i).(j) <- !p;
            incr p
        done
    done;
    paires

let paires = 
    (* cree des couleurs et toutes les paires *)
    cree_couleurs ();
    cree_paires ()

let couleur texte fond =
    attron (A.color_pair paires.(texte).(fond))

(* affiche un pixel *)
let putpixel col x y =
    couleur col col;
    assert (mvaddch y x (int_of_char ' '))

(* on peut alors dessiner directement *)
let ligne_horiz col x1 x2 y =
    for x = x1 to x2 do
        putpixel col x y
    done

let ligne_vert col x y1 y2 =
    for y = y1 to y2 do
        putpixel col x y
    done

let boite col x1 y1 x2 y2 =
    ligne_horiz col x1 x2 y1;
    ligne_horiz col x1 x2 y2;
    ligne_vert col x1 y1 y2;
    ligne_vert col x2 y1 y2

let _ =
    attroff(A.color);
    let h, w = get_size () in
        let continue = ref true in
    let frames = ref 0 in

    let t_x = ref (w/2) in
    let t_y = ref (h/2) in

    (* boucle principale *)
    while !continue do
        (* le clear permet de ne pas avoir de problèmes avec les animations
           mais c'est lent. Dans beaucoup d'application il vaut mieux
           réecrire par dessus ce qui a changé *)
        clear ();

        (* une jolie boite autour de la fenetre *)
        couleur blanc noir;
        border 0 0 0 0 0 0 0 0;

        (* nos boites sont plus moches... mais on peut faire la meme avec plus de
           boulot *)
        boite rouge_clair 1 1 (w/2-1) (h/2-1);
        (* un petit degradé animé *)
        for i = 2 to w/2-2 do
            for j = 2 to h/2-2 do
                putpixel ( (i+j+ !frames) mod ncolors ) i j
            done
        done;

        (* un sinus animé *)
        let dx = 10. *. float_of_int (!frames mod 100) /. 100. in
        for i = 2 to w/2 do
            let t = float_of_int (i-2) /. float_of_int (w/2) in
            let x = 2.0 *. acos (-1.) *. (t +. dx) in
            let y = sin x in
            let j = h/2 + 2 + int_of_float 
                ( float_of_int (h/4) *. ((y +. 1.)/. 2.) ) in
            putpixel blanc i j
        done;


        (* on écrit un texte qui peut se déplacer avec
           les fléches *)
        couleur blanc noir;
        ignore (mvaddstr !t_y !t_x (Printf.sprintf "Texte en %dx%d a deplacer avec les fleches" !t_x !t_y));

        incr frames;

        (* on attend un peu 1/10s *)
        Unix.sleepf 0.05;
        (* on rafraichit l'écran *)
        assert(refresh ());

        (* on regarde si on a appuyé sur une touche *)
        let c = getch () in
        if c >= 0
        then begin
            (* c'est le cas on fait une action en conséquence *)
            (* attention certaines touches sont spéciales et ne
               peuvent pas être converties en caractère comme les
               touches fléchées *)
            if c = Key.down then t_y := min (!t_y+1) (h-2)
            else if c = Key.up then t_y := max (!t_y-1) 1
            else if c = Key.left then t_x := max (!t_x-1) 1 
            else if c = Key.right then t_x := min (!t_x+1) (w-2)
            else (match char_of_int c with
                (* des caractères normaux *)
                | 'q' -> continue := false
                | _ -> ())
        end
    done;

    endwin ()
