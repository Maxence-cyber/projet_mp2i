open Graphics
(* Documentation de graphics ici :

    http://pauillac.inria.fr/~remy/poly/ocaml/htmlman/libref/Graphics.html
*)

let _ =
    open_graph " ";

    let c = ref 0 in

    let frame = ref 0 in
    let t_x = ref 250 in
    let t_y = ref 250 in

    while true do
        incr frame;
        clear_graph ();

        set_color !c;
        incr c;
        moveto (Random.int 100) (Random.int 100);
        lineto (Random.int 100) (Random.int 100);

        set_color black;
        moveto !t_x !t_y;
        draw_string "Deplacez moi avec zqsd";

        if key_pressed()
        then begin
            match read_key () with
            | 'q' -> t_x := !t_x - 10
            | 'd' -> t_x := !t_x + 10
            | 'z' -> t_y := !t_y + 10
            | 's' -> t_y := !t_y - 10
            | _ -> ()
        end;
        (* une petite attente pour ralentir l'animation *)
        Unix.sleepf 0.05
    done

